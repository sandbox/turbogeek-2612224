<?php
/**
 * @file
 * Implements TransFirst payment services for use with Drupal Commerce.
 */


/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_transfirst_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['transfirst'] = array(
    'base' => 'commerce_transfirst',
    'title' => t('TransFirst - Credit Card'),
    'short_title' => t('TransFirst'),
    'display_title' => t('Credit Card'),
    'description' => t('Integrates TransFirst payment gateway for credit card transactions.'),
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_transfirst_settings_form($settings = NULL) {
  $form = array();
  $form['transfirst_gateway_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Gateway ID'),
    '#default_value' => $settings['transfirst_gateway_id'],
    '#description' => t('The Gateway ID assigned to your TransFirst account.'),
    '#required' => TRUE,
  );
  $form['transfirst_reg_key'] = array(
    '#type' => 'textfield',
    '#title' => t('RegKey'),
    '#default_value' => $settings['transfirst_reg_key'],
    '#description' => t('The Registration Key assigned to your TransFirst account.'),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Payment method callback: checkout form.
 */
function commerce_transfirst_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  return commerce_payment_credit_card_form();
}

/**
 * Payment method callback: checkout form validation.
 */
function commerce_transfirst_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  // Validate the credit card fields.
  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );

  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    return FALSE;
  }
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_transfirst_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  // Build data array.
  $data = array(
    // Merchant info.
    'GatewayID' => $payment_method['settings']['transfirst_gateway_id'],
    'RegKey' => $payment_method['settings']['transfirst_reg_key'],
    'IndustryCode' => 2, // eCommerce
    // Credit card info.
    'AccountNumber' => $pane_values['credit_card']['number'],
    //'CVV2' => $pane_values['credit_card']['code'],
    'ExpirationDate' => drupal_substr($pane_values['credit_card']['exp_year'], -2, 2) . drupal_substr(0 . $pane_values['credit_card']['exp_month'], -2, 2),
    // Order info.
    'PONumber' => $order->order_number,
    'CustRefID' => $order->uid,
    'Amount' => commerce_currency_amount_to_decimal($charge['amount'], $charge['currency_code']),
  );

  // Build a description for the order.
  $description = array();

  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if ($line_item_wrapper->type->value() == 'product') {
      $description[] = round($line_item_wrapper->quantity->value()) . 'x ' . $line_item_wrapper->line_item_label->value();
    }
  }

  // Prepare the billing address for request.
  $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();

  // Add additional info.
  $data += array(
    'FullName' => drupal_substr($billing_address['first_name'] . ' ' . $billing_address['last_name'], 0, 50),
    'Address1' => drupal_substr($billing_address['thoroughfare'], 0, 60),
    'Address2' => drupal_substr($billing_address['premise'], 0, 60),
    'City' => drupal_substr($billing_address['locality'], 0, 40),
    'State' => drupal_substr($billing_address['administrative_area'], 0, 40),
    'Zip' => drupal_substr($billing_address['postal_code'], 0, 20),
    'Email' => drupal_substr($order->mail, 0, 255),
  );

  // Submit the request to TransFirst.
  $response = commerce_transfirst_request($data);

  // Set response variables.
  $response_code = $response['ResponseCode'];
  $response_error_code = isset($response['ErrorCode']) ? check_plain($response['ErrorCode']) : '';
  $response_error_msg = isset($response['ErrorMessage']) ? check_plain($response['ErrorMessage']) : '';

  // Prepare transaction object.
  $transaction = commerce_payment_transaction_new('transfirst', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->payload[REQUEST_TIME] = $response;

  // Transaction successful.
  if ($response_code == 00) {
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  } else {
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
  }

  // Add response message.
  $transaction->message = ($response_code == 00 ? t('ACCEPTED') : t('REJECTED')) . ' ' . $response_error_msg;

  // Save the transaction information.
  commerce_payment_transaction_save($transaction);

  // Display an error and rebuild the form.
  if (!empty($response_error_code)) {
    drupal_set_message(t('We received the following error processing your card. Please enter you information again or try a different card.'), 'error');
    drupal_set_message($response_error_code, 'error');
    return FALSE;
  }

}

/**
 * Submits an payment request to TransFirst
 *
 * @param $data
 *   The payment array associated with this request.
 */
function commerce_transfirst_request($data) {
  $response = array();
  // POST request.
  $post_url = 'https://post.transactionexpress.com/PostMerchantService.svc/CreditCardSale';
  $options = array(
    'method' => 'POST',
    'data' => http_build_query($data, '', '&'),
    'timeout' => 60,
  );
  $request = drupal_http_request($post_url, $options);

  // Request has response.
  if (!empty($request)) {
    // Remove quotes.
    $request_clean = str_replace('"', '', $request->data);
    parse_str($request_clean, $response);
  }

  return $response;
}