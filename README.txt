DESCRIPTION
===========

This module adds support for TransFirst as a payment method for Drupal Commerce.


CONFIGURATION
=============

Enable the Rule for TransFirst via Store > Configuration > Payment settings and edit the action to use your TransFirst gateway settings.


AUTHOR
============

Shane Graham "turbogeek" (http://drupal.org/user/266729)
http://www.deckfifty.com